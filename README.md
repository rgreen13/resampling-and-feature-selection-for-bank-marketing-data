# Analysis and Prediction of Customer Behavior on Bank Marketing Sector

In the bank marketing field, it is very challenging to identify individuals who are more likely to do business with the bank. In this paper, we explored and evaluated eight classification models on a bank marketing dataset trying several resampling and feature selection techniques to improve the campaign’s effectiveness. To deal with class imbalance of the dataset, we investigated four resampling techniques and determined the best method for the data. Also, four different feature selection techniques have been considered, which play an important role in deciding the relevant features in the dataset. Based on the selected features, we have evaluated and compared multiple prediction models to identify the best model in predicting the promising customers for subscribing a term deposit. The results of our analysis showed that Gradient Boosting using SMOTE was the best model for this data as it resulted in the highest F-1 (0.655 ± 0.004) and ROC-AUC scores (0.950 ± 0.0004) amongst all eight models with the features selected from Extra Tree Classifier method.

## Structure and Contents
- Dataset is available in the `data` directory
- Work related to the evaluation and analysis of the data is found in the `notebooks` directory
- A presentation regarding the work is contained in the `presentation` directory
